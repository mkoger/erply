<?php

// Kui pole tegu kreeditarve päringuga, siis funktsioon katkestada
if((int)$_POST["invoice_type_id"] != 8){
	return false;
}

session_start();
include("EAPI.class.php");

$api = new EAPI();
$api->clientCode = 389223;
$api->username = "Meelis";
$api->password = "Meelis1243";
$api->url = "https://".$api->clientCode.".erply.com/api/";

$writeOffInput = array(
	"reasonID" => 4,
	"warehouseID" => 1
);
$nr = 1;

// Kõik kreeditarvel defineeritud tooted ja kogused määratakse mahakandmisele
while($nr < 8){
	if(isSet($_POST["product_id".$nr]) && (int)$_POST["amount".$nr] < 0){
		$writeOffInput["productID".$nr] = (int)$_POST["product_id".$nr];
		$writeOffInput["amount".$nr] =  -(int)$_POST["amount".$nr];
		$nr ++;
	} else{
		break;
	}
}

$result = $api->sendRequest("saveInventoryWriteOff", $writeOffInput);